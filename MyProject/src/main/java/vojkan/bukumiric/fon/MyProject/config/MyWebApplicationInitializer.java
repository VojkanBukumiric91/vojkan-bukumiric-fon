package vojkan.bukumiric.fon.MyProject.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	
	public MyWebApplicationInitializer() {
		System.out.println("Initializer");
	}
	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return new Class[] {MyDatabaseConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// TODO Auto-generated method stub
		return new Class[] {MyWebContextConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		// TODO Auto-generated method stub
		return new String[] {"/"};
	}

//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		System.out.println("========================================================");
//		System.out.println("============= MyWebApplicationIznitializer =============");
//		System.out.println("========================================================");
//
//		AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
//		webApplicationContext.register(MyWebContextConfig.class);
//		webApplicationContext.setServletContext(servletContext);
//
//		ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("myDispatcherServlet",
//				new DispatcherServlet(webApplicationContext));
//		dispatcherServlet.addMapping("/");
//		dispatcherServlet.setLoadOnStartup(1);
//	}

}
