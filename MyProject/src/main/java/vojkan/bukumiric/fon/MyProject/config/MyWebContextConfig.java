package vojkan.bukumiric.fon.MyProject.config;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import vojkan.bukumiric.fon.MyProject.formater.CityDtoFormater;
import vojkan.bukumiric.fon.MyProject.formater.ExamDtoFormater;
import vojkan.bukumiric.fon.MyProject.formater.ProfessorDtoFormater;
import vojkan.bukumiric.fon.MyProject.formater.ReelectionDateFormater;
import vojkan.bukumiric.fon.MyProject.formater.StudentDtoFormater;
import vojkan.bukumiric.fon.MyProject.formater.SubjectDtoFormater;
import vojkan.bukumiric.fon.MyProject.formater.TitleDtoFormater;
import vojkan.bukumiric.fon.MyProject.service.CityService;
import vojkan.bukumiric.fon.MyProject.service.ExamService;
import vojkan.bukumiric.fon.MyProject.service.ProfessorService;
import vojkan.bukumiric.fon.MyProject.service.StudentService;
import vojkan.bukumiric.fon.MyProject.service.SubjectService;
import vojkan.bukumiric.fon.MyProject.service.TitleService;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "vojkan.bukumiric.fon.MyProject.controller",
		"vojkan.bukumiric.fon.MyProject.formater" })

public class MyWebContextConfig implements WebMvcConfigurer {

	private CityService cityService;
	private TitleService titleService;
	private SubjectService subjectService;
	private ProfessorService professorService;
	private ExamService examService;
	private StudentService studentService;

	@Autowired
	public MyWebContextConfig(CityService cityService, TitleService titleService, SubjectService subjectService,
			ProfessorService professorService, ExamService examService, StudentService studentService) {
		this.cityService = cityService;
		this.titleService = titleService;
		this.subjectService = subjectService;
		this.professorService = professorService;
		this.examService = examService;
		this.studentService = studentService;
		System.out.println("=================================================================");
		System.out.println("==================== MyWebContextConfig =========================");
		System.out.println("=================================================================");
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		//registry.addViewController("/").setViewName("index");
		registry.addViewController("/").setViewName("redirect:/authentication/login");
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new CityDtoFormater(cityService));
		registry.addFormatter(new TitleDtoFormater(titleService));
		registry.addFormatter(new ReelectionDateFormater(simpleDateFormat()));
		registry.addFormatter(new SubjectDtoFormater(subjectService));
		registry.addFormatter(new ProfessorDtoFormater(professorService));
		registry.addFormatter(new ExamDtoFormater(examService));
		registry.addFormatter(new StudentDtoFormater(studentService));
	}

}
