package vojkan.bukumiric.fon.MyProject.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import vojkan.bukumiric.fon.MyProject.model.UserDto;
import vojkan.bukumiric.fon.MyProject.service.UserService;

@Controller
@RequestMapping(value = "authentication")
public class AuthenticationController {
	private final UserService userService;
	
	
	@Autowired
	public AuthenticationController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping(value = "/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("/authentication/login");
		modelAndView.addObject("userDto", new UserDto());
		return modelAndView;
	}

	@PostMapping(value = "/login")
	public ModelAndView confirm(@Valid @ModelAttribute ("userDto") UserDto userDto, BindingResult result, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		userService.findUserByNameAndPassword(userDto);
		if(result.hasErrors()) {
			modelAndView.setViewName("/authentication/login");
			
			modelAndView.addObject("userDto", userDto);
		} else {
			
			HttpSession session = request.getSession(false);
			session.setAttribute("user", userDto);
			modelAndView.setViewName("index");
		}
		return modelAndView;
	}

	@ModelAttribute(name = "userDto")
	public UserDto generateUserDto() {
		return new UserDto();
	}

}
