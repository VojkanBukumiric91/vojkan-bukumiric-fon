package vojkan.bukumiric.fon.MyProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import vojkan.bukumiric.fon.MyProject.model.ExamDto;
import vojkan.bukumiric.fon.MyProject.model.ProfessorDto;
import vojkan.bukumiric.fon.MyProject.model.SubjectDto;
import vojkan.bukumiric.fon.MyProject.service.ExamService;
import vojkan.bukumiric.fon.MyProject.service.ProfessorService;
import vojkan.bukumiric.fon.MyProject.service.SubjectService;

@Controller
@RequestMapping(value = "/exam")
public class ExamController {
	private final ExamService examService;
	private final SubjectService subjectService;
	private final ProfessorService professorService;

	@Autowired
	public ExamController(ExamService examService, SubjectService subjectService, ProfessorService professorService) {
		this.examService = examService;
		this.subjectService = subjectService;
		this.professorService = professorService;
	}

	@GetMapping(value="home")
	public ModelAndView home() {
		System.out.println("====================================================================");
		System.out.println("====================ExamController: home()  ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("exam/home");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("==================== ExamController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("exam/add");
		ExamDto examDto = new ExamDto();
		modelAndView.addObject("examDto", examDto);
		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "examDto") ExamDto examDto, BindingResult result) {
		System.out.println("================================  =================================");
		System.out.println(examDto);
		System.out.println("================================  =================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("================================ NOT OK =================================");
			modelAndView.setViewName("exam/add");
			modelAndView.addObject("examDto", examDto);
		} else {
			System.out.println("================================     OK =================================");
			modelAndView.setViewName("exam/all");
			examService.save(examDto);
		}
		return modelAndView;
	}

	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("exam/all");
		return modelAndView;
	}

	@ModelAttribute(name = "examDto")
	public ExamDto examDto() {
		return new ExamDto();
	}

	@ModelAttribute(name = "exams")
	public List<ExamDto> exams() {
		return examService.findAll();
	}

	@ModelAttribute(name = "subjects")
	public List<SubjectDto> subjects() {
		return subjectService.findAll();
	}

	@ModelAttribute(name = "professors")
	public List<ProfessorDto> professors() {
		return professorService.findAll();
	}

}
