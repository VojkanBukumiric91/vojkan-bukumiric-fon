package vojkan.bukumiric.fon.MyProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import vojkan.bukumiric.fon.MyProject.model.ExamDto;
import vojkan.bukumiric.fon.MyProject.model.ExamRegistrationDto;
import vojkan.bukumiric.fon.MyProject.model.StudentDto;
import vojkan.bukumiric.fon.MyProject.service.ExamRegistrationService;
import vojkan.bukumiric.fon.MyProject.service.ExamService;
import vojkan.bukumiric.fon.MyProject.service.StudentService;

@Controller
@RequestMapping("examRegistration")
public class ExamRegistrationController {

	private final ExamRegistrationService examRegistrationService;
	private final ExamService examService;
	private final StudentService studentService;

	@Autowired
	public ExamRegistrationController(ExamRegistrationService examRegistrationService, ExamService examService,
			StudentService studentService) {
		this.examRegistrationService = examRegistrationService;
		this.examService = examService;
		this.studentService = studentService;
	}

	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("====================================================================");
		System.out.println("====================ExamRegistrationController: home()  ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("examRegistration/home");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("==================== ExamRegistrationController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("examRegistration/add");
		ExamRegistrationDto examRegistrationDto = new ExamRegistrationDto();
		modelAndView.addObject("examDto", examRegistrationDto);
		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView save(
			@Valid @ModelAttribute(name = "examRegistrationDto") ExamRegistrationDto examRegistrationDto,
			BindingResult result) {
		System.out.println("================================  =================================");
		System.out.println(examRegistrationDto);
		System.out.println("================================  =================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("================================ NOT OK =================================");
			modelAndView.setViewName("examRegistration/add");
			modelAndView.addObject("examDto", examRegistrationDto);
		} else {
			System.out.println("================================     OK =================================");
			modelAndView.setViewName("examRegistration/all");
			examRegistrationService.save(examRegistrationDto);
		}
		return modelAndView;
	}

	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("examRegistration/all");
		return modelAndView;
	}
	@ModelAttribute(name="examRegistrations")
	public List<ExamRegistrationDto>examRegistrations(){
		return examRegistrationService.findAll();
	}

	@ModelAttribute(name = "exams")
	public List<ExamDto> exams() {
		return examService.findAll();
	}

	@ModelAttribute(name = "students")
	public List<StudentDto> students() {
		return studentService.findAll();
	}

	@ModelAttribute(name = "examRegistrationDto")
	public ExamRegistrationDto examRegistrationDto() {
		return new ExamRegistrationDto();
	}

}
