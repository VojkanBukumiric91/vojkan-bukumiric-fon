package vojkan.bukumiric.fon.MyProject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import vojkan.bukumiric.fon.MyProject.model.CityDto;
import vojkan.bukumiric.fon.MyProject.model.StudentDto;
import vojkan.bukumiric.fon.MyProject.service.CityService;
import vojkan.bukumiric.fon.MyProject.service.StudentService;
import vojkan.bukumiric.fon.MyProject.validator.StudentDtoValidator;

@Controller
@RequestMapping(value = "/student")
public class StudentController {

	private final StudentService studentService;
	private final CityService cityService;

	@Autowired
	public StudentController(StudentService studentService, CityService cityService) {
		this.studentService = studentService;
		this.cityService = cityService;
	}

	@GetMapping(value="home")
	public String home() {
		System.out.println("====================================================================");
		System.out.println("====================StudentController: home()  ===================");
		System.out.println("====================================================================");
		return "student/home";
	}

	@GetMapping(value = "add")
	public ModelAndView modelAndView(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("====================================================================");
		System.out.println("==================== StudentController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("student/add");
		StudentDto studentDto = new StudentDto();
		modelAndView.addObject("studentDto", studentDto);
		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "studentDto") StudentDto studentDto, BindingResult result) {
		System.out.println("================================  =================================");
		System.out.println(studentDto);
		System.out.println("================================  =================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("================================ NOT OK =================================");
			modelAndView.setViewName("student/add");
			modelAndView.addObject("studentDto", studentDto);
		} else {
			System.out.println("================================     OK =================================");
			modelAndView.setViewName("student/home");
			studentService.save(studentDto);
		}
		return modelAndView;
	}

	@GetMapping("/details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/details");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}

	@GetMapping(value = "/edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/edit");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}

	@PostMapping(value = "/update")
	public ModelAndView update(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("student/edit");
			modelAndView.addObject("studentDto", studentDto);
		} else {
			studentService.save(studentDto);
			modelAndView.setViewName("student/all");
		}
		return modelAndView;
	}

	@GetMapping(value = "/remove")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/all");
		studentService.deleteById(id);
		return modelAndView;
	}

	@GetMapping(value = "/all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("student/all");
		return modelAndView;
	}

	@ModelAttribute(value = "cities")
	public List<CityDto> cities() {
		return cityService.findAll();
	}

	@ModelAttribute(value = "students")
	public List<StudentDto> students() {
		return studentService.findAll();
	}

	@ModelAttribute(value = "studentDto")
	public StudentDto studentDto() {
		return new StudentDto();

	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new StudentDtoValidator());
	}

}
