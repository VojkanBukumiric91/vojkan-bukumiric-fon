package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.City;
import vojkan.bukumiric.fon.MyProject.model.CityDto;

@Component
public class CityConverter {

	public CityDto entityToDto(City city) {
		return new CityDto(city.getId(), city.getNumber(), city.getName());
	}

	public City dtoToEntity(CityDto cityDto) {
		return new City(cityDto.getId(), cityDto.getNumber(), cityDto.getName());
	}
	
	public List<City> convertDtoListToEntityList(List<CityDto> dtoList) {
		List<City> cityEntityList = new ArrayList<City>();
		for(CityDto cityDto: dtoList) {
			cityEntityList.add(dtoToEntity(cityDto));
		}
		return cityEntityList;
	}
	public List<CityDto> convertEntityListToDtoList(List<City> entityList) {
		List<CityDto> cityDtoList = new ArrayList<CityDto>();
		for(City city: entityList) {
			cityDtoList.add(entityToDto(city));
		}
		return cityDtoList;
	}

}
