package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.Exam;
import vojkan.bukumiric.fon.MyProject.model.ExamDto;

@Component
public class ExamConverter {
	private final SubjectConverter subjectConverter;
	private final ProfessorConverter professorConverter;

	@Autowired
	public ExamConverter(SubjectConverter subjectConverter, ProfessorConverter professorConverter) {
		this.subjectConverter = subjectConverter;
		this.professorConverter = professorConverter;
	}

	public ExamDto entityToDto(Exam exam) {
		return new ExamDto(exam.getId(), subjectConverter.entityToDto(exam.getSubject()),
				professorConverter.entityToDto(exam.getProfessor()), exam.getDate());
	}

	public Exam dtoToEntity(ExamDto examDto) {
		return new Exam(examDto.getId(), subjectConverter.dtoToEntity(examDto.getSubjectDto()),
				professorConverter.dtoToEntity(examDto.getProfessorDto()), examDto.getDate());
	}

	public List<Exam> convertDtoListToEntityList(List<ExamDto> dtoList) {
		List<Exam> examEntityList = new ArrayList<Exam>();
		for (ExamDto examDto : dtoList) {
			examEntityList.add(dtoToEntity(examDto));
		}
		return examEntityList;
	}

	public List<ExamDto> convertEntityListToDtoList(List<Exam> entityList) {
		List<ExamDto> examDtoList = new ArrayList<ExamDto>();
		for (Exam exam : entityList) {
			examDtoList.add(entityToDto(exam));

		}
		return examDtoList;
	}

}
