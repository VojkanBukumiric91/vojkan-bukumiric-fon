package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.ExamRegistration;
import vojkan.bukumiric.fon.MyProject.model.ExamRegistrationDto;

@Component
public class ExamRegistrationConverter {

	private final ExamConverter examConverter;
	private final StudentConverter studentConverter;

	@Autowired
	public ExamRegistrationConverter(ExamConverter examConverter, StudentConverter studentConverter) {
		this.examConverter = examConverter;
		this.studentConverter = studentConverter;
	}

	public ExamRegistrationDto entityToDto(ExamRegistration examRegistration) {
		return new ExamRegistrationDto(examRegistration.getId(), examConverter.entityToDto(examRegistration.getExam()),
				studentConverter.entityToDto(examRegistration.getStudent()));
	}

	public ExamRegistration dtoToEntity(ExamRegistrationDto examRegistrationDto) {

		return new ExamRegistration(examRegistrationDto.getId(),
				examConverter.dtoToEntity(examRegistrationDto.getExamDto()),
				studentConverter.dtoToEntity(examRegistrationDto.getStudentDto()));
	}

	public List<ExamRegistration> convertDtoListToEntityList(List<ExamRegistrationDto> dtoList) {
		List<ExamRegistration> examRegEntityList = new ArrayList<ExamRegistration>();
		for (ExamRegistrationDto examRegistration : dtoList) {
			examRegEntityList.add(dtoToEntity(examRegistration));
		}

		return examRegEntityList;
	}

	public List<ExamRegistrationDto> convertEntityListToDtoList(List<ExamRegistration> entityList) {
		List<ExamRegistrationDto> examRegDtoList = new ArrayList<ExamRegistrationDto>();
		for (ExamRegistration examRegistration : entityList) {
			examRegDtoList.add(entityToDto(examRegistration));
		}
		return examRegDtoList;
	}

}
