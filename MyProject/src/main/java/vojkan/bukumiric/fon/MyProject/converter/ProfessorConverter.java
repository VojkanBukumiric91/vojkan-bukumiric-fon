package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.Professor;
import vojkan.bukumiric.fon.MyProject.model.ProfessorDto;

@Component
public class ProfessorConverter {
	private final CityConverter cityConverter;
	private final TitleConverter titleConverter;

	@Autowired
	public ProfessorConverter(CityConverter cityConverter, TitleConverter titleConverter) {
		this.cityConverter = cityConverter;
		this.titleConverter = titleConverter;
	}

	public ProfessorDto entityToDto(Professor professor) {

		return new ProfessorDto(professor.getId(), professor.getFirstname(), professor.getLastname(),
				professor.getEmail(), professor.getAddress(), cityConverter.entityToDto(professor.getCity()),
				professor.getPhone(), professor.getReelectionDate(), titleConverter.entityToDto(professor.getTitle()));
	}

	public Professor dtoToEntity(ProfessorDto professorDto) {
		return new Professor(professorDto.getId(), professorDto.getFirstname(), professorDto.getLastname(),
				professorDto.getEmail(), professorDto.getAddress(),
				cityConverter.dtoToEntity(professorDto.getCityDto()), professorDto.getPhone(),
				professorDto.getReelectionDate(), titleConverter.dtoToEntity(professorDto.getTitleDto()));
	}

	public List<Professor> convertDtoListToEntityList(List<ProfessorDto> dtoList) {
		List<Professor> professorEntityList = new ArrayList<Professor>();
		for (ProfessorDto professorDto : dtoList) {
			professorEntityList.add(dtoToEntity(professorDto));
		}
		return professorEntityList;
	}

	public List<ProfessorDto> convertEntityListToDtoList(List<Professor> entityList) {
		List<ProfessorDto> professorDtoList = new ArrayList<ProfessorDto>();
		for (Professor professor : entityList) {
			professorDtoList.add(entityToDto(professor));
		}
		return professorDtoList;
	}

}
