package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.Student;
import vojkan.bukumiric.fon.MyProject.model.StudentDto;

@Component
public class StudentConverter {
	private final CityConverter cityConverter;

	@Autowired
	public StudentConverter(CityConverter cityConverter) {
		this.cityConverter = cityConverter;
	}

	public StudentDto entityToDto(Student student) {
		return new StudentDto(student.getId(), student.getIndexNumber(), student.getFirstName(), student.getLastName(),
				student.getEmail(), student.getAddress(), student.getPhone(),
				cityConverter.entityToDto(student.getCity()), student.getCurrentYearOfStudy());
	}

	public Student dtoToEntity(StudentDto studentDto) {
		return new Student(studentDto.getId(), studentDto.getIndexNumber(), studentDto.getFirstName(),
				studentDto.getLastName(), studentDto.getEmail(), studentDto.getAddress(), studentDto.getPhone(),
				cityConverter.dtoToEntity(studentDto.getCityDto()), studentDto.getCurrentYearOfStudy());
	}

	public List<Student> convertDtoListToEntityList(List<StudentDto> dtoList) {
		List<Student> studentEntityList = new ArrayList<Student>();
		for (StudentDto studentDto : dtoList) {
			studentEntityList.add(dtoToEntity(studentDto));
		}
		return studentEntityList;
	}

	public List<StudentDto> convertEntityListToDtoList(List<Student> entityList) {
		List<StudentDto> studentDtoList = new ArrayList<StudentDto>();
		for (Student student : entityList) {
			studentDtoList.add(entityToDto(student));
		}
		return studentDtoList;
	}
}
