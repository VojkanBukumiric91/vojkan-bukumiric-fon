package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.Subject;
import vojkan.bukumiric.fon.MyProject.model.SubjectDto;

@Component
public class SubjectConverter {

	public SubjectDto entityToDto(Subject subject) {
		return new SubjectDto(subject.getId(), subject.getName(), subject.getDescription(), subject.getYearOfStudy(),
				subject.getSemester());
	}

	public Subject dtoToEntity(SubjectDto subjectDto) {
		return new Subject(subjectDto.getId(), subjectDto.getName(), subjectDto.getDescription(),
				subjectDto.getYearOfStudy(), subjectDto.getSemester());
	}

	public List<SubjectDto> convertEntityListToDtoList(List<Subject> entityList) {
		List<SubjectDto> subjectDtoList = new ArrayList<SubjectDto>();
		for (Subject subject : entityList) {
			subjectDtoList.add(entityToDto(subject));
		}
		return subjectDtoList;
	}

	public List<Subject> convertDtoListToEntityList(List<SubjectDto> dtoList) {
		List<Subject> subjectEntityList = new ArrayList<Subject>();
		for (SubjectDto subjectDto : dtoList) {
			subjectEntityList.add(dtoToEntity(subjectDto));
		}
		return subjectEntityList;
	}

}
