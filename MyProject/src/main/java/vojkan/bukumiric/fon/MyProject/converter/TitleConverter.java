package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.Title;
import vojkan.bukumiric.fon.MyProject.model.TitleDto;

@Component
public class TitleConverter {
	
	public TitleDto entityToDto(Title title) {
		return new TitleDto(title.getId(), title.getType());
	}
	public Title dtoToEntity(TitleDto titleDto) {
		return new Title(titleDto.getId(), titleDto.getType());
	}
	public List<Title> convertDtoListToEntityList(List<TitleDto>dtoList){
		List<Title> titleEntityList = new ArrayList<Title>();
		for (TitleDto titleDto : dtoList) {
			titleEntityList.add(dtoToEntity(titleDto));
		}
		return titleEntityList;
	}
	public List<TitleDto>convertEntityListToDtoList(List<Title>entityList){
		List<TitleDto> titleDtoList = new ArrayList<TitleDto>();
		for (Title title : entityList) {
			titleDtoList.add(entityToDto(title));
		}
		return titleDtoList;
	}

}
