package vojkan.bukumiric.fon.MyProject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.entity.User;
import vojkan.bukumiric.fon.MyProject.model.UserDto;

@Component
public class UserConverter {

	public UserDto entityToDto(User user) {
		return new UserDto(user.getId(), user.getUsername(), user.getPassword());
	}

	public User dtoToEntity(UserDto userDto) {
		return new User(userDto.getId(), userDto.getUsername(), userDto.getPassword());
	}

	public List<UserDto> convertEntityListToDtoList(List<User> entityList) {
		List<UserDto> usersDtoList = new ArrayList<UserDto>();
		for (User user : entityList) {
			usersDtoList.add(entityToDto(user));
		}
		return usersDtoList;
	}

	public List<User> convertDtoListToEntityList(List<UserDto> dtoList) {
		List<User> userEntityList = new ArrayList<User>();
		for (UserDto userDto : dtoList) {
			userEntityList.add(dtoToEntity(userDto));
		}
		return userEntityList;
	}

}
