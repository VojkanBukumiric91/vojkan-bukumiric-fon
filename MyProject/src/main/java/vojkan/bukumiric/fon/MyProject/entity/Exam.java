package vojkan.bukumiric.fon.MyProject.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "exam")
public class Exam {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "subjectId", referencedColumnName = "id")
	private Subject subject;
	@ManyToOne
	@JoinColumn(name = "professorId", referencedColumnName = "id") 
	private Professor professor;
	@Temporal(TemporalType.DATE)
	private Date date;
	
	public Exam() {
		
	}

	public Exam(Long id, Subject subject, Professor professor, Date date) {
		super();
		this.id = id;
		this.subject = subject;
		this.professor = professor;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Exam [id=" + id + ", subject=" + subject + ", professor=" + professor + ", date=" + date + "]";
	}
	
	
	

	
}
