package vojkan.bukumiric.fon.MyProject.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="examRegistration")
public class ExamRegistration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name="examId",referencedColumnName = "id")
	private Exam exam;
	@ManyToOne
	@JoinColumn(name="studentId",referencedColumnName = "id")
	private Student student;
	
	public ExamRegistration() {
		
	}

	public ExamRegistration(Long id, Exam exam, Student student) {
		this.id = id;
		this.exam = exam;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "ExamRegistration [id=" + id + ", exam=" + exam + ", student=" + student + "]";
	}
	
	

}
