package vojkan.bukumiric.fon.MyProject.entity;

public enum Semester {
	Summer("Summer"), Winter("Winter");
	
	private String type;

	private Semester(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
