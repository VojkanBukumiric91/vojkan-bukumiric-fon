package vojkan.bukumiric.fon.MyProject.formater;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.model.CityDto;
import vojkan.bukumiric.fon.MyProject.service.CityService;

@Component
public class CityDtoFormater implements Formatter<CityDto> {
	
	private final CityService cityService;
	
	
	@Autowired
	public CityDtoFormater(CityService cityService) {
		super();
		this.cityService = cityService;
	}

	@Override
	public String print(CityDto cityDto, Locale locale) {
		System.out.println("PRINT");
		System.out.println(cityDto);
		
		return cityDto.toString();
	}

	@Override
	public CityDto parse(String text, Locale locale) throws ParseException {
		System.out.println("PARSE");
		Long id=Long.parseLong(text);
		CityDto cityDto = cityService.findById(id);
		System.out.println(cityDto);
		return cityDto;
	}
	
	

}
