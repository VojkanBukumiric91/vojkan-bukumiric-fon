package vojkan.bukumiric.fon.MyProject.formater;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.model.ExamDto;
import vojkan.bukumiric.fon.MyProject.service.ExamService;

@Component
public class ExamDtoFormater implements Formatter<ExamDto> {

	private final ExamService examService;

	@Autowired
	public ExamDtoFormater(ExamService examService) {
		this.examService = examService;
	}

	@Override
	public String print(ExamDto object, Locale locale) {

		return object.toString();
	}

	@Override
	public ExamDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ExamDto examDto = examService.findById(id);
		return examDto;
	}

}
