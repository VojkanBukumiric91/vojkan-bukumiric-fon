package vojkan.bukumiric.fon.MyProject.formater;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.model.ProfessorDto;
import vojkan.bukumiric.fon.MyProject.service.ProfessorService;

@Component
public class ProfessorDtoFormater implements Formatter<ProfessorDto> {

	private final ProfessorService professorService;

	@Autowired
	public ProfessorDtoFormater(ProfessorService professorService) {
		this.professorService = professorService;
	}

	@Override
	public String print(ProfessorDto object, Locale locale) {

		return object.toString();
	}

	@Override
	public ProfessorDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ProfessorDto professorDto = professorService.findById(id);
		return professorDto;
	}

}
