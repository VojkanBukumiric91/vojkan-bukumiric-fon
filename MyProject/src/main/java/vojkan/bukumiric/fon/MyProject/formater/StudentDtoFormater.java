package vojkan.bukumiric.fon.MyProject.formater;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import vojkan.bukumiric.fon.MyProject.model.StudentDto;
import vojkan.bukumiric.fon.MyProject.service.StudentService;

@Component
public class StudentDtoFormater implements Formatter<StudentDto> {

	private final StudentService studentService;

	@Autowired
	public StudentDtoFormater(StudentService studentService) {
		this.studentService = studentService;
	}

	@Override
	public String print(StudentDto object, Locale locale) {

		return object.toString();
	}

	@Override
	public StudentDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		StudentDto studentDto = studentService.findById(id);
		return studentDto;
	}

}
