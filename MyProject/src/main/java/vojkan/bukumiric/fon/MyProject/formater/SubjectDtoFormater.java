package vojkan.bukumiric.fon.MyProject.formater;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import vojkan.bukumiric.fon.MyProject.model.SubjectDto;
import vojkan.bukumiric.fon.MyProject.service.SubjectService;

public class SubjectDtoFormater implements Formatter<SubjectDto> {

	private final SubjectService subjectService;

	@Autowired
	public SubjectDtoFormater(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Override
	public String print(SubjectDto subjectDto, Locale locale) {

		return subjectDto.toString();
	}

	@Override
	public SubjectDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		return subjectService.findById(id);
	}

}
