package vojkan.bukumiric.fon.MyProject.formater;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import vojkan.bukumiric.fon.MyProject.model.TitleDto;
import vojkan.bukumiric.fon.MyProject.service.TitleService;

public class TitleDtoFormater implements Formatter<TitleDto> {
	private final TitleService titleService;
	
	@Autowired
	public TitleDtoFormater(TitleService titleService) {
		this.titleService = titleService;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {
		
		return titleDto.toString();
	}

	@Override
	public TitleDto parse(String text, Locale locale) throws ParseException {
		Long id  = Long.parseLong(text);
		TitleDto titleDto = titleService.findById(id);
		return titleDto;
	}

}
