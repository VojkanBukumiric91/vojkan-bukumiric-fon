package vojkan.bukumiric.fon.MyProject.model;

import java.io.Serializable;

public class CityDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long number;
	private String name;

	public CityDto() {

	}

	public CityDto(Long id, Long number, String name) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityDto [id=" + id + ", number=" + number + ", name=" + name + "]";
	}

}
