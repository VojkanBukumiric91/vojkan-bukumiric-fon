package vojkan.bukumiric.fon.MyProject.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class ExamRegistrationDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	@NotNull(message = "Exam can not be empty! please choose exam")
	private ExamDto examDto;
	@NotNull(message = "Student can not be empty! please choose student")
	private StudentDto studentDto;
	
	public ExamRegistrationDto() {
		
	}

	public ExamRegistrationDto(Long id, ExamDto examDto, StudentDto studentDto) {
		this.id = id;
		this.examDto = examDto;
		this.studentDto = studentDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExamDto getExamDto() {
		return examDto;
	}

	public void setExamDto(ExamDto examDto) {
		this.examDto = examDto;
	}

	public StudentDto getStudentDto() {
		return studentDto;
	}

	public void setStudentDto(StudentDto studentDto) {
		this.studentDto = studentDto;
	}

	@Override
	public String toString() {
		return "ExamRegistrationDto [id=" + id + ", examDto=" + examDto + ", studentDto=" + studentDto + "]";
	}
	
	

}
