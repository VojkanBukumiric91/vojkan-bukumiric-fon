package vojkan.bukumiric.fon.MyProject.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class ProfessorDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	@NotNull(message = "First Name can not be empty")
	@Length(min = 3, message = "Minimum number of characters for Firstname is 3")
	private String firstname;
	@NotNull(message = "Last Name can not be empty")
	@Length(min = 3, message = "Minimum number of characters for Lastname is 3")
	private String lastname;
	private String email;
	private String address;
	private CityDto cityDto;
	private String phone;
	@NotNull(message = "ReelectionDate can not be empty")
	private Date reelectionDate;
	private TitleDto titleDto;

	public ProfessorDto() {

	}

	public ProfessorDto(Long id, String firstname, String lastname, String email, String address, CityDto cityDto,
			String phone, Date reelectionDate, TitleDto titleDto) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.titleDto = titleDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleDto getTitleDto() {
		return titleDto;
	}

	public void setTitleDto(TitleDto titleDto) {
		this.titleDto = titleDto;
	}

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", address=" + address + ", cityDto=" + cityDto + ", phone=" + phone + ", reelectionDate="
				+ reelectionDate + ", titleDto=" + titleDto + "]";
	}

	public String getFullname() {
		return firstname + " " + lastname;
	}

}
