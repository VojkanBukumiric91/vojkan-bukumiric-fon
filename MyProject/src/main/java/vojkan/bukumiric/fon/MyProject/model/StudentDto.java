package vojkan.bukumiric.fon.MyProject.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class StudentDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "IndexNumber can not be a empty!")
	@Length(min = 10, message = "Minimum number of characters for IndexNumber is 10")
	private String indexNumber;
	@NotNull(message = "FirstName can not be a empty!")
	@Length(min = 3, message = "Minimum number of characters for First Name is 3")
	private String firstName;
	@NotNull(message = "LastName can not be a empty")
	@Length(min = 3, message = "Minimum number of characters for Last Name is 3")
	private String lastName;
	private String email;
	private String address;
	private String phone;
	private CityDto cityDto;
	@NotNull
	private int currentYearOfStudy;

	public StudentDto() {

	}

	public StudentDto(Long id, String indexNumber, String firstName, String lastName, String email, String address,
			String phone, CityDto cityDto, int currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.cityDto = cityDto;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", address=" + address + ", phone=" + phone + ", cityDto=" + cityDto
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}

	public String getFullname() {
		return firstName + " " + lastName;
	}

}
