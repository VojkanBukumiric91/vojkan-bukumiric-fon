package vojkan.bukumiric.fon.MyProject.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import vojkan.bukumiric.fon.MyProject.entity.Semester;

public class SubjectDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Name can not be empty")
	@Length(min = 3, message = "Minimal number of characters for name is 3")
	private String name;
	private String description;
	private int yearOfStudy;
	private Semester semester;

	public SubjectDto() {

	}

	public SubjectDto(Long id, String name, String description, int yearOfStudy, Semester semester) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	@Override
	public String toString() {
		return "SubjectDto [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + "]";
	}

}
