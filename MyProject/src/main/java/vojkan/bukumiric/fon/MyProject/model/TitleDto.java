package vojkan.bukumiric.fon.MyProject.model;

import java.io.Serializable;

public class TitleDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String type;
	public TitleDto() {
		
	}
	public TitleDto(Long id, String type) {
		super();
		this.id = id;
		this.type = type;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "TitleDto [id=" + id + ", type=" + type + "]";
	}
	

}
