package vojkan.bukumiric.fon.MyProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vojkan.bukumiric.fon.MyProject.entity.Exam;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long> {

}
