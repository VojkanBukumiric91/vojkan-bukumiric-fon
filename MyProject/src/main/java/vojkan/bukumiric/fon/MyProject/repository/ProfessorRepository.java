package vojkan.bukumiric.fon.MyProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vojkan.bukumiric.fon.MyProject.entity.Professor;

public interface ProfessorRepository extends JpaRepository<Professor, Long> {

}
