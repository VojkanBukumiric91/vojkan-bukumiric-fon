package vojkan.bukumiric.fon.MyProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vojkan.bukumiric.fon.MyProject.entity.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	
	 

}
