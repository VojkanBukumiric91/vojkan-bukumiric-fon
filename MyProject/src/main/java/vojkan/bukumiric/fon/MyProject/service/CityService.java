package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.CityDto;

public interface CityService {
	public void save(CityDto cityDto);
	public List<CityDto>findAll();
	public CityDto findById(Long id);
	
}
