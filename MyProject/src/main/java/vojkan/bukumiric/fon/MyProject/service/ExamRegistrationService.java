package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.ExamRegistrationDto;

public interface ExamRegistrationService {
	public void save(ExamRegistrationDto examRegistrationDto);
	public List<ExamRegistrationDto>findAll();
	public ExamRegistrationDto findById(Long id);

}
