package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.ExamDto;

public interface ExamService {
	
	public void save(ExamDto examDto);
	public List<ExamDto>findAll();
	public ExamDto findById(Long id);
}
