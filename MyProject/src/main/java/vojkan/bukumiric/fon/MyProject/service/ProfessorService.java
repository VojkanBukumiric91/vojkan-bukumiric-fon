package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.ProfessorDto;

public interface ProfessorService {
	public void save(ProfessorDto professorDto);
	public List<ProfessorDto>findAll();
	public ProfessorDto findById(Long id);
	public void deleteById(Long id);
}
