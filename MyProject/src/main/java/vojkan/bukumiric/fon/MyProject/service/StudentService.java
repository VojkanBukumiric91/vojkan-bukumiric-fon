package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.StudentDto;

public interface StudentService {
	public void save (StudentDto studentDto);
	public List<StudentDto>findAll();
	public StudentDto findById(Long id);
	public void deleteById(Long id);
}
