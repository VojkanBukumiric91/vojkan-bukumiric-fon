package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.SubjectDto;

public interface SubjectService {
	
	public void save(SubjectDto subjectDto);
	public List<SubjectDto>findAll();
	public SubjectDto findById(Long id);
	public void deleteById(Long id);
}
