package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.TitleDto;

public interface TitleService {
	public void save(TitleDto titleDto);
	public List<TitleDto>findAll();
	public TitleDto findById(Long id);
}
