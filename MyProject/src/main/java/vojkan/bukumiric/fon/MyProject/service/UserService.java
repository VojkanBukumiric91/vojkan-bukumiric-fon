package vojkan.bukumiric.fon.MyProject.service;

import java.util.List;

import vojkan.bukumiric.fon.MyProject.model.UserDto;

public interface UserService {
	public List<UserDto> findUserByNameAndPassword(UserDto userDto) throws Exception;
}
