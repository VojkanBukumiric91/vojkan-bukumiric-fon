package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.CityConverter;
import vojkan.bukumiric.fon.MyProject.entity.City;
import vojkan.bukumiric.fon.MyProject.model.CityDto;
import vojkan.bukumiric.fon.MyProject.repository.CityRepository;
import vojkan.bukumiric.fon.MyProject.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {

	private CityRepository cityRepository;
	private CityConverter cityConverter;

	@Autowired
	public CityServiceImpl(CityRepository cityRepository, CityConverter cityConverter) {
		this.cityRepository = cityRepository;
		this.cityConverter = cityConverter;
	}

	@Override
	public void save(CityDto cityDto) {
		cityRepository.save(cityConverter.dtoToEntity(cityDto));

	}

	@Override
	public List<CityDto> findAll() {
		List<City> cities = cityRepository.findAll();
		List<CityDto> listCitiesDto = cityConverter.convertEntityListToDtoList(cities);
		return listCitiesDto;
	}

	@Override
	public CityDto findById(Long id) {
		Optional<City> optionalCity = cityRepository.findById(id);
		if (optionalCity.isPresent()) {
			return cityConverter.entityToDto(optionalCity.get());
		} else {
			return null;
		}
	}

}
