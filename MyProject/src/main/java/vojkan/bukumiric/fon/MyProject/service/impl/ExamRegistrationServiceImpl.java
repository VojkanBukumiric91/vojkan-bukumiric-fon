package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.ExamRegistrationConverter;
import vojkan.bukumiric.fon.MyProject.entity.ExamRegistration;
import vojkan.bukumiric.fon.MyProject.model.ExamRegistrationDto;
import vojkan.bukumiric.fon.MyProject.repository.ExamRegistrationRepository;
import vojkan.bukumiric.fon.MyProject.service.ExamRegistrationService;

@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService {

	private final ExamRegistrationRepository examRegistrationRepository;
	private final ExamRegistrationConverter examRegistrationConverter;

	@Autowired
	public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository,
			ExamRegistrationConverter examRegistrationConverter) {
		this.examRegistrationRepository = examRegistrationRepository;
		this.examRegistrationConverter = examRegistrationConverter;

	}

	@Override
	public void save(ExamRegistrationDto examRegistrationDto) {
		examRegistrationRepository.save(examRegistrationConverter.dtoToEntity(examRegistrationDto));
	}

	@Override
	public List<ExamRegistrationDto> findAll() {
		List<ExamRegistration> listExamRegistrationEntity = examRegistrationRepository.findAll();
		List<ExamRegistrationDto> listExamRegistrationDto = examRegistrationConverter
				.convertEntityListToDtoList(listExamRegistrationEntity);
		return listExamRegistrationDto;
	}

	@Override
	public ExamRegistrationDto findById(Long id) {
		Optional<ExamRegistration> examRegistrationOptional = examRegistrationRepository.findById(id);
		if (examRegistrationOptional.isPresent()) {
			return examRegistrationConverter.entityToDto(examRegistrationOptional.get());
		} else {
			return null;
		}
	}

}
