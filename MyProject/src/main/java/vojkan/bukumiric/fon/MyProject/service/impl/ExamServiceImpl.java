package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.ExamConverter;
import vojkan.bukumiric.fon.MyProject.entity.Exam;
import vojkan.bukumiric.fon.MyProject.model.ExamDto;
import vojkan.bukumiric.fon.MyProject.repository.ExamRepository;
import vojkan.bukumiric.fon.MyProject.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService {

	private final ExamRepository examRepository;
	private final ExamConverter examConverter;

	@Autowired
	public ExamServiceImpl(ExamRepository examRepository, ExamConverter examConverter) {
		this.examRepository = examRepository;
		this.examConverter = examConverter;
	}

	@Override
	public void save(ExamDto examDto) {
		examRepository.save(examConverter.dtoToEntity(examDto));

	}

	@Override
	public List<ExamDto> findAll() {
		List<Exam> exams = examRepository.findAll();
		List<ExamDto> dtoExams = examConverter.convertEntityListToDtoList(exams);

		return dtoExams;
	}

	@Override
	public ExamDto findById(Long id) {
		Optional<Exam> examOptional = examRepository.findById(id);
		if (examOptional.isPresent()) {
			return examConverter.entityToDto(examOptional.get());
		} else {
			return null;
		}
	}

}
