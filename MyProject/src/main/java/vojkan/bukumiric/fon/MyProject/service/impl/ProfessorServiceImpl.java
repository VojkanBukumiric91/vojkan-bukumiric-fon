package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.ProfessorConverter;
import vojkan.bukumiric.fon.MyProject.entity.Professor;
import vojkan.bukumiric.fon.MyProject.model.ProfessorDto;
import vojkan.bukumiric.fon.MyProject.repository.ProfessorRepository;
import vojkan.bukumiric.fon.MyProject.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {

	private ProfessorRepository professorRepository;
	private ProfessorConverter professorConverter;

	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository, ProfessorConverter professorConverter) {
		this.professorRepository = professorRepository;
		this.professorConverter = professorConverter;
	}

	@Override
	public void save(ProfessorDto professorDto) {
		professorRepository.save(professorConverter.dtoToEntity(professorDto));

	}

	@Override
	public List<ProfessorDto> findAll() {
		List<Professor> professors = professorRepository.findAll();
		List<ProfessorDto> dtoProfessors = professorConverter.convertEntityListToDtoList(professors);
		return dtoProfessors;
	}

	@Override
	public ProfessorDto findById(Long id) {
		Optional<Professor> professorOpt = professorRepository.findById(id);
		if (professorOpt.isPresent()) {
			return professorConverter.entityToDto(professorOpt.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		professorRepository.deleteById(id);

	}

}
