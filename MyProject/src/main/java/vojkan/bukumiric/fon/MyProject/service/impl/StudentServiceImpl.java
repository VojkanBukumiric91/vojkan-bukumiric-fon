package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.StudentConverter;
import vojkan.bukumiric.fon.MyProject.entity.Student;
import vojkan.bukumiric.fon.MyProject.model.StudentDto;
import vojkan.bukumiric.fon.MyProject.repository.StudentRepository;
import vojkan.bukumiric.fon.MyProject.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	private final StudentRepository studentRepository;
	private StudentConverter studentConverter;
	

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository,StudentConverter studentConverter) {
		this.studentRepository = studentRepository;
		this.studentConverter=studentConverter;
	}

	@Override
	public void save(StudentDto studentDto) {
		studentRepository.save(studentConverter.dtoToEntity(studentDto));
	
	}

	@Override
	public List<StudentDto> findAll() {
		List<Student> students = studentRepository.findAll();
		List<StudentDto> listStudentsDto= studentConverter.convertEntityListToDtoList(students);
		
		return listStudentsDto;
	}

	@Override
	public StudentDto findById(Long id) {
		Optional<Student> studentOpt= studentRepository.findById(id);
		if(studentOpt.isPresent()) {
			return studentConverter.entityToDto(studentOpt.get());
		}else {
			return null;
		}
	}

	

	@Override
	public void deleteById(Long id) {
		studentRepository.deleteById(id);

	}

}
