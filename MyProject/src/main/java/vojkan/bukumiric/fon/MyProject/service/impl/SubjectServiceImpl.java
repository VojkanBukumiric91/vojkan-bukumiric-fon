package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.SubjectConverter;
import vojkan.bukumiric.fon.MyProject.entity.Subject;
import vojkan.bukumiric.fon.MyProject.model.SubjectDto;
import vojkan.bukumiric.fon.MyProject.repository.SubjectRepository;
import vojkan.bukumiric.fon.MyProject.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {
	private final SubjectRepository subjectRepository;
	private SubjectConverter subjectConverter;

	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository, SubjectConverter subjectConverter) {
		this.subjectRepository = subjectRepository;
		this.subjectConverter = subjectConverter;
	}

	@Override
	public void save(SubjectDto subjectDto) {
		subjectRepository.save(subjectConverter.dtoToEntity(subjectDto));
	}

	@Override
	public List<SubjectDto> findAll() {
		List<Subject> subjects = subjectRepository.findAll();
		List<SubjectDto> listSubjectsDto = subjectConverter.convertEntityListToDtoList(subjects);
		return listSubjectsDto;
	}

	@Override
	public SubjectDto findById(Long id) {
		Optional<Subject> subjectOpt = subjectRepository.findById(id);
		if (subjectOpt.isPresent()) {
			return subjectConverter.entityToDto(subjectOpt.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		subjectRepository.deleteById(id);

	}

}
