package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.TitleConverter;
import vojkan.bukumiric.fon.MyProject.entity.Title;
import vojkan.bukumiric.fon.MyProject.model.TitleDto;
import vojkan.bukumiric.fon.MyProject.repository.TitleRepository;
import vojkan.bukumiric.fon.MyProject.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {

	private TitleRepository titleRepository;
	private TitleConverter titleConverter;

	@Autowired
	public TitleServiceImpl(TitleRepository titleRepository, TitleConverter titleConverter) {
		this.titleRepository = titleRepository;
		this.titleConverter = titleConverter;
	}

	@Override
	public void save(TitleDto titleDto) {
		titleRepository.save(titleConverter.dtoToEntity(titleDto));

	}

	@Override
	public List<TitleDto> findAll() {
		List<Title> listTitle = titleRepository.findAll();
		List<TitleDto> listTitleDto = titleConverter.convertEntityListToDtoList(listTitle);
		return listTitleDto;
	}

	@Override
	public TitleDto findById(Long id) {
		Optional<Title> optionalTitle = titleRepository.findById(id);
		if (optionalTitle.isPresent()) {
			return titleConverter.entityToDto(optionalTitle.get());
		} else {
			return null;
		}
	}

}
