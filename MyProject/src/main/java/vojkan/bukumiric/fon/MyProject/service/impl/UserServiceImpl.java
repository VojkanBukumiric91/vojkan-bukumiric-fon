package vojkan.bukumiric.fon.MyProject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vojkan.bukumiric.fon.MyProject.converter.UserConverter;
import vojkan.bukumiric.fon.MyProject.entity.User;
import vojkan.bukumiric.fon.MyProject.model.UserDto;
import vojkan.bukumiric.fon.MyProject.repository.UserRepository;
import vojkan.bukumiric.fon.MyProject.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	private final UserRepository userRepository;
	private final UserConverter userConverter;
	
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}


	@Override
	public List<UserDto> findUserByNameAndPassword(UserDto userDto) throws Exception {
		List<User> users = userRepository.findByUsernameAndPassword(userDto.getUsername(), userDto.getPassword());
		List<UserDto> listUserDto = userConverter.convertEntityListToDtoList(users);
		if(listUserDto.isEmpty()) {
			throw new Exception("User not exist");
		}
		return listUserDto;
	}


	


	


	

}
