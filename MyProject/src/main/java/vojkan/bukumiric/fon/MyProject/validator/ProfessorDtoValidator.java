package vojkan.bukumiric.fon.MyProject.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import vojkan.bukumiric.fon.MyProject.model.ProfessorDto;

public class ProfessorDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return ProfessorDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProfessorDto professorDto =(ProfessorDto)target;
		
		if(professorDto.getEmail()!=null) {
			if(!professorDto.getEmail().contains("@")) {
				errors.rejectValue("email", "ProfessorDto.email", "Email must contains @");
			}
		}
		if(professorDto.getEmail().isEmpty()) {
			errors.rejectValue("email", "ProfessorDto.email", "Email can not be empty");
		}
		if(professorDto.getPhone()!=null) {
			if(professorDto.getPhone().length()<6) {
				errors.rejectValue("phone", "ProfessorDto.phone", "Minimal number for phone is 6");
			}
		}
		if(professorDto.getAddress()!=null) {
			if(professorDto.getAddress().length()<3) {
				errors.rejectValue("address", "ProfessorDto.address", "Minimal number characters for address is 3");
			}
		}
		
	}

}
