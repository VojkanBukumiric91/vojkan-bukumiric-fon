<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<style type="text/css">
.error {
	color: red;
}
body{
    background-color:#5286F3;
    font-size:14px;
    color:#fff;
}
.simple-login-container{
    width:300px;
    max-width:100%;
    margin:50px auto;
}
.simple-login-container h2{
    text-align:center;
    font-size:20px;
}

.simple-login-container .btn-login{
    background-color:#FF5964;
    color:#fff;
}
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

	<form:form action="/MyProject/authentication/login" method="post"
		modelAttribute="userDto">
		<div class="simple-login-container">
    <h2>Login Form</h2>
    <div class="row">
        <div class="col-md-12 form-group">
		Username:<form:input type="text" path="username" id="usernameId" />
		   </div>
    </div>
    
		<br />
		<form:errors path="username" cssClass="error" />
		<p />
		<div class="row">
        <div class="col-md-12 form-group">
		Password:<form:input type="password" path="password" id="passwordId" />
		<br />
		<form:errors path="password" cssClass="error" />
		<p />
			   </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
		<button id="Login">Login</button>
		</div>
			   </div>
    </div>
	</form:form>
	

</body>
</html>