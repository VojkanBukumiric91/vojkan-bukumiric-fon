<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add exam:</title>
<style type="text/css">
.error {
	color: red;
}

body {
	padding-top: 56px;
	display: flex;
	flex-direction: column;
	height: 100vh;
}

.fill {
	flex: 1;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
						<h2 class="card-title">Add Exam</h2>
						<form:form class="horizontal"
							action="${pageContext.request.contextPath}/exam/save"
							method="post" modelAttribute="examDto">
						<div class="form-group">
								Subject:<br>
								<div class="col-sm-8">
									<form:label path="subjectDto" />
									<form:select path="subjectDto">
										<form:options items="${subjects}" itemValue="id"
											itemLabel="name" />
									</form:select>
									<form:errors path="subjectDto" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group">
								Professor:<br>
								<div class="col-sm-8">

									<form:label path="professorDto" />
									<form:select path="professorDto">
										<form:options items="${professors}" itemValue="id"
											itemLabel="fullname" />
									</form:select>
									<form:errors path="professorDto" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group">
								Date:<br>
								<div class="col-sm-8">
									<form:input type="date" path="date" id="dateId" />
									<br />
									<form:errors path="date" cssClass="error" />

									<p />
								</div>
							</div>


							<button id="save">Save</button>
						</form:form>
			</div>
</body>
</html>