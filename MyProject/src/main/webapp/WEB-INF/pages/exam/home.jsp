<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home page for exam</title>
</head>
<body>
<div>
		<c:url value="/exam/add" var="examAdd"></c:url>
		<a href="<c:out value="${examAdd}"/>">Add exam</a>
	</div>

	<div>
		<c:url value="/exam/all" var="examAll"></c:url>
		<a href="<c:out value="${examAll}"/>">All exam</a>
	</div>
	

</body>
</html>