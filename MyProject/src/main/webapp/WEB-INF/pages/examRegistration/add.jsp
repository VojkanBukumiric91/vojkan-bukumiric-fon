<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add exam Registration</title>
<style type="text/css">
.error {
	color: red;
}

body {
	padding-top: 56px;
	display: flex;
	flex-direction: column;
	height: 100vh;
}

.fill {
	flex: 1;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container fill">
		<div class="row">
			<div class="col-md-7 offset-3">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Register Exam</h2>

						<form:form action="/MyProject/examRegistration/save" method="post"
							modelAttribute="examRegistrationDto">
							<div class="form-group row">
								Exam:
								<div class="col-sm-8">
									<form:label path="examDto" />
									<form:select path="examDto">
										<form:options items="${exams}" itemValue="id"
											itemLabel="subjectDto.name" />
									</form:select>
									<form:errors path="examDto" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-8">
									Student:

									<form:label path="studentDto" />
									<form:select path="studentDto">
										<form:options items="${students}" itemValue="id"
											itemLabel="fullname" />
									</form:select>
									<form:errors path="studentDto" cssClass="error" />
									<p />
								</div>
							</div>




							<button id="save">Save</button>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>