<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Exam registration home page</title>
</head>
<body>
<div>
		<c:url value="/examRegistration/add" var="examRegistrationAdd"></c:url>
		<a href="<c:out value="${examRegistrationAdd}"/>">Add exam registration</a>
	</div>

	<div>
		<c:url value="/examRegistration/all" var="examRegistrationAll"></c:url>
		<a href="<c:out value="${examRegistrationAll}"/>">All exam registration</a>
	</div>
	

</body>
</html>