<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>index jsp.page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<style>
	.nav-item{
	padding: 10px;
	}
	.navbar-nav{
	text-align: center;
	}
	</style>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-light">
		<ul class="navbar-nav">
			<li class="nav-item"><c:url value="/student/home"
					var="studentHome"></c:url> <a
				href="<c:out value="${studentHome}"/>">Home page for student</a></li>
				
			<li class="nav-item"><c:url value="/professor/home"
					var="professorHome"></c:url> <a
				href="<c:out value="${professorHome}"/>">Home page for
					professors</a></li>
			<li class="nav-item"><c:url value="/subject/home"
					var="subjectHome"></c:url> <a
				href="<c:out value="${subjectHome}"/>">Home page for subject</a></li>

			<li class="nav-item"><c:url value="/exam/home" var="examHome"></c:url>
				<a href="<c:out value="${examHome}"/>">Home page for exam</a></li>

			<li class="nav-item"><c:url value="/examRegistration/home"
					var="examRegistrationHome"></c:url> <a
				href="<c:out value="${examRegistrationHome}"/>">Home page for
					exam registration</a></li>

		</ul>
	</nav>

</body>
</html>