<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All Professors</title>
<style>

</style>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
<div class="container">
		<table class="table table-dark table-striped">
			<tbody>
				<tr>
					<th>First name</th>
					<th>Last name</th>
					<th>Email</th>
					<th>Address</th>
					<th>City</th>
					<th>Phone</th>
					<th>Reelection date</th>
					<th>Title</th>
					<th>Details</th>
					<th>Edit</th>
					<th>Remove</th>
				</tr>

				<c:forEach items="${professors}" var="professors">

					<c:url value="/professor/details" var="professorDetails">
						<c:param name="id" value="${professor.id}"></c:param>
					</c:url>

					<c:url value="/professor/edit" var="professorEdit">
						<c:param name="id" value="${professor.id}"></c:param>
					</c:url>

					<c:url value="professor/remove" var="studentRemove">
						<c:param name="id" value="${professor.id}"></c:param>
					</c:url>


					<tr>
						<td>${professors.firstname}</td>
						<td>${professors.lastname}</td>
						<td>${professors.email}</td>
						<td>${professors.address}</td>
						<td>${professors.cityDto.name}</td>
						<td>${professors.phone}</td>
						<td>${professors.reelectionDate}</td>
						<td>${professors.titleDto.type}</td>
						<td><a href="${professorDetails}">Details</a></td>
						<td><a href="${professorEdit}">Edit</a></td>
						<td><a href="${professorRemove}">Remove</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>