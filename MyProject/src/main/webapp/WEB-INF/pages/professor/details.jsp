<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
<style>
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="container">
		<table class="table table-dark table-striped">
			<tbody>
				<tr>
					<th>First name</th>
					<th>Last name</th>
					<th>Email</th>
					<th>Address</th>
					<th>City</th>
					<th>Phone</th>
					<th>Reelection date</th>
					<th>Title</th>

				</tr>

				<tr>
					<td>${professorDto.firstname}</td>
					<td>${professorDto.lastname}</td>
					<td>${professorDto.email}</td>
					<td>${professorDto.address}</td>
					<td>${professorDto.cityDto.name}</td>
					<td>${professorDto.phone}</td>
					<td>${professorDto.reelectionDate}</td>
					<td>${professorDto.titleDto.type}</td>

				</tr>

			</tbody>
		</table>
	</div>
</body>
</html>