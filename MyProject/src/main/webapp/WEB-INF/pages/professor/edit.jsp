<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit Professor</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style type="text/css">
.error {
	color: red;
}

body {
	padding-top: 56px;
	display: flex;
	position: relative;
	flex-direction: column;
	height: 100vh;
}

.fill {
	flex: 1;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="container fill">
		<div class="row">
			<div class="col-md-7 offset-3">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h4 class="card-title">Edit Professor</h4>

						<form:form action="/MyProject/professor/update" method="post"
							modelAttribute="professorDto">
							<form:hidden path="id" id="id" />
							<div class="form-group row">
								First Name:
								<div class="col-sm-8">
									<form:input type="text" path="firstname" id="firstnameId" />
									<br />
									<form:errors path="firstname" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Last Name:
								<div class="col-sm-8">
									<form:input type="text" path="lastname" id="lastnameId" />
									<br />
									<form:errors path="lastname" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Email:
								<div class="col-sm-8">
									<form:input type="text" path="email" id="emailId" />
									<br />
									<form:errors path="email" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Address:
								<div class="col-sm-8">
									<form:input type="text" path="address" id="addressId" />
									<br />
									<form:errors path="address" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								City:
								<div class="col-sm-8">
									<form:label path="cityDto" />
									<form:select path="cityDto">
										<form:option value="" label="Choose City"></form:option>
										<form:options items="${cities}" itemValue="id"
											itemLabel="name" />
									</form:select>
									<form:errors path="cityDto" cssClass="error" />
								</div>
							</div>
							<p />
							<div class="form-group row">
								Phone:
								<div class="col-sm-8">
									<form:input type="text" path="phone" id="phoneId" />
									<br />
									<form:errors path="phone" cssClass="error" />
									<p />

								</div>
							</div>

							<div class="form-group row">
								Reelection date:
								<div class="col-sm-8">
									<form:input type="date" path="reelectionDate"
										id="reelectionDateId" />
									<br />
									<form:errors path="reelectionDate" cssClass="error" />
									<p />

								</div>
							</div>
							<div class="form-group row">
								Title:
								<div class="col-sm-8">
									<form:label path="titleDto" />
									<form:select path="titleDto">
										<!--  	<form:option value="" label="Choose City"></form:option>-->
										<form:options items="${titles}" itemValue="id"
											itemLabel="type" />
									</form:select>
									<form:errors path="titleDto" cssClass="error" />
									<br />
								</div>
							</div>
							<button id="save">Save</button>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>