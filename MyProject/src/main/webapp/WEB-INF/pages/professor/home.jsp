<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Professor home page</title>
</head>
<body>
<div>
		<c:url value="/professor/add" var="professorAdd"></c:url>
		<a href="<c:out value="${professorAdd}"/>">Add professors</a>
	</div>

	<div>
		<c:url value="/professor/all" var="professorAll"></c:url>
		<a href="<c:out value="${professorAll}"/>">All professors</a>
	</div>
	<div>
		<c:url value="/professor/details" var="professorDetails"></c:url>
		<a href="<c:out value="${professorDetails}"/>">Details professors</a>
	</div>

</body>
</html>