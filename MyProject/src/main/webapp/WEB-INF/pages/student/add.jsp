<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add:student</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
.error {
	color: red;
}
body {
  padding-top: 56px;
    display: flex;
    position: relative;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;
}

</style>
</head>
<body>
	<div class="container fill">
		<div class="row">
			<div class="col-md-7 offset-3">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Add Student</h2>
						<form:form action="/MyProject/student/save" method="post"
							modelAttribute="studentDto">
							<div class="form-group row">
								IndexNumber:<br>
								<div class="col-sm-8">
									<form:input type="text" path="indexNumber" id="indexNumberId" />
									<br />
									<form:errors path="indexNumber" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								FirstName:<br>
								<div class="col-sm-8">
									<form:input type="text" path="firstName" id="firstnameId" />
									<br />
									<form:errors path="firstName" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								LastName:<br>
								<div class="col-sm-8">
									<form:input type="text" path="lastName" id="lastnameId" />
									<br />
									<form:errors path="lastName" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Email:<br>
								<div class="col-sm-8">
									<form:input type="text" path="email" id="emailId" />
									<br />
									<form:errors path="email" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Address:<br>
								<div class="col-sm-8">
									<form:input type="text" path="address" id="addressId" />
									<br />
									<form:errors path="address" cssClass="error" />
								</div>
							</div>
							<div class="form-group row">
								City:<br>
								<div class="col-sm-8">
									<form:label path="cityDto" />
									<form:select path="cityDto">
										<form:option value="" label="Choose City"></form:option>
										<form:options items="${cities}" itemValue="id"
											itemLabel="name" />
									</form:select>
									<form:errors path="cityDto" cssClass="error" />
								</div>
							</div>
							<p />
							<div class="form-group row">
								Phone:<br>
								<div class="col-sm-8">
									<form:input type="text" path="phone" id="phoneId" />
									<br />
									<form:errors path="phone" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Current Year of Study:<br>
								<div class="col-sm-8">
									<form:input type="text" path="currentYearOfStudy"
										id="currentYearOfStudyId" />
									<br />
									<form:errors path="currentYearOfStudy" cssClass="error" />
								</div>
							</div>
							<button id="save">Save</button>
						</form:form>

					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>