<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All students</title>
<style>

</style>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="container">

		<table class="table table-dark table-striped">
			<tbody>
				<tr>
					<th>IndexNumber</th>
					<th>FirstName</th>
					<th>LastName</th>
					<th>Email</th>
					<th>Address</th>
					<th>City</th>
					<th>Phone</th>
					<th>Current year</th>
					<th>Details</th>
					<th>Edit</th>
					<th>Remove</th>
				</tr>

				<c:forEach items="${students}" var="student">

					<c:url value="/student/details" var="studentDetails">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>

					<c:url value="/student/edit" var="studentEdit">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>

					<c:url value="/student/remove" var="studentRemove">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>


					<tr>
						<td>${student.indexNumber}</td>
						<td>${student.firstName}</td>
						<td>${student.lastName}</td>
						<td>${student.email}</td>
						<td>${student.address}</td>
						<td>${student.cityDto.name}</td>
						<td>${student.phone}</td>
						<td>${student.currentYearOfStudy}</td>
						<td><a href="${studentDetails}">Details</a></td>
						<td><a href="${studentEdit}">Edit</a></td>
						<td><a href="${studentRemove}">Remove</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>