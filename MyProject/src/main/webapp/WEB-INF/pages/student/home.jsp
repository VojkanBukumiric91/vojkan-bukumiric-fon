<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student: home page</title>
</head>
<body>
<div>
		<c:url value="/student/add" var="studentAdd"></c:url>
		<a href="<c:out value="${studentAdd}"/>">Add student</a>
	</div>

	<div>
		<c:url value="/student/all" var="studentAll"></c:url>
		<a href="<c:out value="${studentAll}"/>">All students</a>
	</div>
	<div>
		<c:url value="/student/details" var="studentDetails"></c:url>
		<a href="<c:out value="${studentDetails}"/>">Details student</a>
	</div>
</body>
</html>