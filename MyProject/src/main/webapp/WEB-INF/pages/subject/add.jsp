<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add subject</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style type="text/css">
.error {
	color: red;
}
body {
  padding-top: 56px;
    display: flex;
    position: relative;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;
}
</style>
</head>
<body>
	<div class="container fill">
		<div class="row">
			<div class="col-md-7 offset-3">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Add Subject</h2>

						<form:form action="/MyProject/subject/save" method="post"
							modelAttribute="subjectDto">
							<div class="form-group row">
								Name:
								<div class="col-sm-8">
									<form:input type="text" path="name" id="nameId" />
									<br />
									<form:errors path="name" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Description:
								<div class="col-sm-8">
									<form:input type="text" path="description" id="descriptionId" />
									<br />
									<form:errors path="description" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Year of study:
								<div class="col-sm-8">
									<form:input type="text" path="yearOfStudy" id="yearOfStudyId" />
									<br />
									<form:errors path="yearOfStudy" cssClass="error" />
									<p />
								</div>
							</div>
							<div class="form-group row">
								Semester:
								<div class="col-sm-8">
									<form:label path="semester" />
									<form:select path="semester">
										<form:option value="" label="Choose Semester"></form:option>
										<form:options items="${semester}" itemValue="type"
											itemLabel="type" />
									</form:select>
									<form:errors path="semester" cssClass="error" />
								</div>
							</div>
							<p />


							<button id="save">Save</button>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>