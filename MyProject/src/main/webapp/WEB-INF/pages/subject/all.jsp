<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All subject</title>
<style>
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="container">
		<table class="table table-dark table-striped">
			<tbody>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Year Of Study</th>
					<th>Semester</th>
					<th>Details</th>
					<th>Edit</th>
					<th>Remove</th>
				</tr>
				<c:forEach items="${subjects}" var="subject">

					<c:url value="/subject/details" var="subjectDetails">
						<c:param name="id" value="${subject.id}"></c:param>
					</c:url>

					<c:url value="/subject/edit" var="subjectEdit">
						<c:param name="id" value="${subject.id}"></c:param>
					</c:url>

					<c:url value="subject/remove" var="subjectRemove">
						<c:param name="id" value="${subject.id}"></c:param>
					</c:url>


					<tr>
						<td>${subject.name}</td>
						<td>${subject.description}</td>
						<td>${subject.yearOfStudy}</td>
						<td>${subject.semester}</td>
						<td><a href="${subjectDetails}">Details</a></td>
						<td><a href="${subjectEdit}">Edit</a></td>
						<td><a href="${subjectRemove}">Remove</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>