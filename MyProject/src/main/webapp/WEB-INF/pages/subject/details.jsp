<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>details subject</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="container">
		<table class="table table-dark table-striped">
		<tbody>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Year Of Study</th>
				<th>Semester</th>

			</tr>
			<tr>
				<td>${subjectDto.name}</td>
				<td>${subjectDto.description}</td>
				<td>${subjectDto.yearOfStudy}</td>
				<td>${subjectDto.semester}</td>

			</tr>
		</tbody>
	</table>
</div>
</body>
</html>