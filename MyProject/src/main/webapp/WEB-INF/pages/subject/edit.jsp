<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<title>Edit subject</title>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<div class="container">
		<form:form action="${pageContext.request.contextPath}/subject/update"
			method="post" modelAttribute="subjectDto">
			<form:hidden path="id" id="id" />
			<div class="form-group">
				Name:<br>
				<form:input type="text" path="name" id="nameId" />
				<br />
				<form:errors path="name" cssClass="error" />
				<p />
			</div>
			<div class="form-group">
				Description:<br>
				<form:input type="text" path="description" id="descriptionId" />
				<br />
				<form:errors path="description" cssClass="error" />
				<p />
			</div>
			<div class="form-group">
				Year of study:<br>
				<form:input type="text" path="yearOfStudy" id="yearOfStudyId" />
				<br />
				<form:errors path="yearOfStudy" cssClass="error" />
				<p />
			</div>
			<div class="form-group">
				Semester:<br>
				<form:label path="semester" />
				<form:select path="semester">
					<form:option value="" label="Choose Semester"></form:option>
					<form:options items="${semester}" itemValue="type" itemLabel="type" />
				</form:select>
				<form:errors path="semester" cssClass="error" />

				<p />
			</div>

			<button id="save">Save</button>
		</form:form>
	</div>
</body>
</html>